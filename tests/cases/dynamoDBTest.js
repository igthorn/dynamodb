
"use strict";

const igthornDynamoDB = require("../../lib/dynamoDB");
const assert          = require("assert");
const config          = require("config");
const awsConfig       = config.get("aws");
const AWS             = require("aws-sdk");
const _               = require("underscore");

const awsCredentails = {
  apiVersion: awsConfig.dynamoDB.apiVersion,
  region: awsConfig.dynamoDB.region,
  accessKeyId: awsConfig.accounts[0].accessKeyId,
  secretAccessKey: awsConfig.accounts[0].secretAccessKey,
  endpoint: awsConfig.dynamoDB.endPoint,
  maxBatchRequestSize: awsConfig.dynamoDB.maxBatchRequestSize
};

describe("IghtornDynamoDB", () => {

  before(() => {
    igthornDynamoDB.connect(awsCredentails);
  });

  beforeEach(function(done) {

    this.timeout(0);

    igthornDynamoDB.getItems("Jobs_test_dev")
      .then((data) => {
        const jobs = [];
        for(let i=0; i<data.Items.length; i++) {
          jobs.push({
            "JobID": data.Items[i].JobID,
            "UserID": data.Items[i].UserID
          });
        }

        return igthornDynamoDB.deleteItems(
          "Jobs_test_dev",
          jobs
        );
      })
      .then(() => {
        return igthornDynamoDB.getItems("Emails_test_dev");
      })
      .then((data) => {
        const emails = [];
        for(let i=0; i<data.Items.length; i++) {
          emails.push({
            "EmailID": data.Items[i].EmailID,
            "UserID": data.Items[i].UserID
          });
        }

        return igthornDynamoDB.deleteItems(
          "Emails_test_dev",
          emails
        );
      })
      .then(() => {
        done();
      })
      .catch((error) => {
        console.log(error);
        throw error;
      })
  });

  it("should be a singleton", () => {
    let igthornDynamoDB1 = require("../../lib/dynamoDB");
    let igthornDynamoDB2 = require("../../lib/dynamoDB");
    assert.ok(igthornDynamoDB1 === igthornDynamoDB2);      
  });  

  describe("connecting twice using same access credentials", () => {	

    it("should give same connection", () => {
      igthornDynamoDB.connect(awsCredentails);
      const dynamoDB1 = igthornDynamoDB.getConnection('main');
      igthornDynamoDB.connect(awsCredentails);
      const dynamoDB2 = igthornDynamoDB.getConnection('main');

      assert.ok(dynamoDB1 === dynamoDB2);
    });

    it("should give same connection(named)", () => {
      igthornDynamoDB.connect(awsCredentails, 'Query');
      const dynamoDB1 = igthornDynamoDB.getConnection('Query');
      igthornDynamoDB.connect(awsCredentails, 'Query');
      const dynamoDB2 = igthornDynamoDB.getConnection('Query');

      assert.ok(dynamoDB1 === dynamoDB2);

      igthornDynamoDB.connect(awsCredentails, 'SomethingElse');
      const dynamoDB3 = igthornDynamoDB.getConnection('SomethingElse');

      assert.ok(dynamoDB2 !== dynamoDB3);
    });

    it("should give same connection (named connections)", () => {
      igthornDynamoDB.connect(awsCredentails, 'aws');
      const dynamoDB1 = igthornDynamoDB.getConnection('aws');
      igthornDynamoDB.connect(awsCredentails, 'aws');
      const dynamoDB2 = igthornDynamoDB.getConnection('aws');

      assert.ok(dynamoDB1 === dynamoDB2);
    });

  });

  describe("getItems", () => {

    it("should allow to query items (callback)", function(done) {

      this.timeout(0);

      const items = [];
      for(let i=1; i<6; i++) {
        items.push({
          "JobID": "JobID" + i,
          "UserID": "UserID" + i,
          "relevancyPosition": i,
          "other": "xyz abc"
        })
      }

      igthornDynamoDB.putItems(
        "Jobs_test_dev",
        items
      ).then(() => {
        igthornDynamoDB.getItems(
          "Jobs_test_dev",
          {
            "relevancyPosition": {
              'GT': 2
            }
          },
          {},
          (error, data) => {
            if(error) {
              throw error;
            }

            assert.deepEqual(data, {
              Count: 3,
              Items: items.slice(2, 5),
              ScannedCount: 5
            });

            done();
          }
        );
      })

    });

    it("should allow to query items (promise)", function(done) {

      this.timeout(0);

      const items = [];
      for(let i=1; i<6; i++) {
        items.push({
          "JobID": "JobID" + i,
          "UserID": "UserID" + i,
          "relevancyPosition": i,
          "other": "xyz abc"
        })
      }

      igthornDynamoDB.putItems(
        "Jobs_test_dev",
        items
      ).then(() => {
        return igthornDynamoDB.getItems(
          "Jobs_test_dev",
          {
            "relevancyPosition": {
              'GT': 2
            }
          }
        );
      })
      .then((data) => {
        assert.deepEqual(data, {
          Count: 3,
          Items: items.slice(2, 5),
          ScannedCount: 5
        });

        done();
      })
      .catch((error) => {
        done(error);
      })
    });

  });

  describe("getItemByKeys", () => {

    it("should allow to retrieve item by keys(primary & secondary) (callback)", function (done) {

      this.timeout(0);

      const items = [];
      for(let i=1; i<6; i++) {
        items.push({
          "JobID": "JobID" + i,
          "UserID": "UserID" + i,
          "relevancyPosition": i,
          "other": "xyz abc"
        })
      }

      igthornDynamoDB.putItems(
        "Jobs_test_dev",
        items
      )
      .then(() => {
        igthornDynamoDB.getItemByKeys(
          "Jobs_test_dev",
          {
            "UserID": "UserID4",
            "JobID": "JobID4"
          },
          {},
          (error, data) => {
            if(error) {
              throw error;
            }

            assert.deepEqual(data, {
              "Responses": {
                "Jobs_test_dev": [
                  {
                    "relevancyPosition": 4,
                    "other": "xyz abc",
                    "UserID": "UserID4",
                    "JobID": "JobID4"
                  }
                ]
              },
              "UnprocessedKeys": {}
            });

            done();
          }
        );
      })
    });

    it("should allow to retrieve item by keys(primary & secondary) (promise)", function (done) {

      this.timeout(0);

      const items = [];
      for(let i=1; i<6; i++) {
        items.push({
          "JobID": "JobID" + i,
          "UserID": "UserID" + i,
          "relevancyPosition": i,
          "other": "xyz abc"
        })
      }

      igthornDynamoDB.putItems(
        "Jobs_test_dev",
        items
      )
      .then(() => {
        return igthornDynamoDB.getItemByKeys(
          "Jobs_test_dev",
          {
            "UserID": "UserID4",
            "JobID": "JobID4"
          }
        );
      })
      .then((data) => {
        assert.deepEqual(data, {
          "Responses": {
            "Jobs_test_dev": [
              {
                "relevancyPosition": 4,
                "other": "xyz abc",
                "UserID": "UserID4",
                "JobID": "JobID4"
              }
            ]
          },
          "UnprocessedKeys": {}
        });
        done();
      })
      .catch((error) => {
        done(error);
      })
    });

  });

  describe("putItem", () => {

    it("should allow insert item (callback)", function(done) {

      this.timeout(0);

      igthornDynamoDB.getItemByKeys(
        "Jobs_test_dev",
        {
          "UserID": "UserID2",
          "JobID": "JobID2"
        }
      ).then((data) => {

        assert.deepEqual(data, {
          "Responses": {
            "Jobs_test_dev": []
          },
          "UnprocessedKeys": {}
        });

        igthornDynamoDB.putItem(
          "Jobs_test_dev",
          {
            "UserID": "UserID2",
            "JobID": "JobID2",
            "name": "some job",
            "other": "xyz"
          },
          {},
          (error, data) => {
            if(error) {
              throw error;
            }

            assert.deepEqual(data, {});

            igthornDynamoDB.getItemByKeys(
              "Jobs_test_dev",
              {
                "UserID": "UserID2",
                "JobID": "JobID2"
              }
            ).then((data) => {

              assert.deepEqual(data, {
                "Responses": {
                  "Jobs_test_dev": [
                    {
                      "UserID": "UserID2",
                      "JobID": "JobID2",
                      "name": "some job",
                      "other": "xyz"
                    }
                  ]
                },
                "UnprocessedKeys": {}
              });

              done();
            })
            .catch((error) => {
              throw error;
            });
          }
        );

      });

    });

    it("should allow insert item (promise)", function(done) {

      this.timeout(0);

      igthornDynamoDB.getItemByKeys(
        "Jobs_test_dev",
        {
          "UserID": "UserID2",
          "JobID": "JobID2"
        }
      ).then((data) => {

        assert.deepEqual(data, {
          "Responses": {
            "Jobs_test_dev": []
          },
          "UnprocessedKeys": {}
        });

        return igthornDynamoDB.putItem(
          "Jobs_test_dev",
          {
            "UserID": "UserID2",
            "JobID": "JobID2",
            "name": "some job",
            "other": "xyz"
          }
        );
      })
      .then((data) => {
        return igthornDynamoDB.getItemByKeys(
          "Jobs_test_dev",
          {
            "UserID": "UserID2",
            "JobID": "JobID2"
          }
        );
      })
      .then((data) => {
        assert.deepEqual(data, {
          "Responses": {
            "Jobs_test_dev": [
              {
                "UserID": "UserID2",
                "JobID": "JobID2",
                "name": "some job",
                "other": "xyz"
              }
            ]
          },
          "UnprocessedKeys": {}
        });

        done();
      })
      .catch((error) => {
        done(error);
      })
    });

  });

  describe("putItems", () => {

    it("should allow batch insert items (callback)", function(done) {

      this.timeout(0);

      const items = [];
      for(let i=1; i<51; i++) {
        items.push({
          "JobID": "putItemsJobID" + String("0" + i).slice(-2),
          "UserID": "putItemsUserID" + String("0" + i).slice(-2),
          "name": "some job xyz",
          "other": "xyz abc"
        });
      }

      igthornDynamoDB.getItems(
        "Jobs_test_dev"
      )
      .then((data) => {
        assert.deepEqual(data, {
          Items: [],
          Count: 0,
          ScannedCount: 0
        });

        igthornDynamoDB.putItems(
          "Jobs_test_dev",
          items,
          {},
          (error, data) => {
            if(error) {
              throw error;
            }

            assert.deepEqual(data, [
              {
                "error": null,
                "data": {
                  "UnprocessedItems": {}
                }
              },
              {
                "error": null,
                "data": {
                  "UnprocessedItems": {}
                }
              }
            ]);

            igthornDynamoDB.getItems(
              "Jobs_test_dev"
            )
            .then((data) => {

              data.Items = _.sortBy(data.Items, 'JobID');

              assert.deepEqual(
                data,
                {
                  Items: items,
                  Count: 50,
                  ScannedCount: 50
                }
              );

              done();
            })
            .catch((error) => {
              done(error);
            });
          }
        );
      })
      .catch((error) => {
        throw new error;
      });
    });

    it("should allow batch insert items (promise)", function (done) {

      this.timeout(0);

      igthornDynamoDB.connect(awsCredentails);

      const items = [];
      for(let i=101; i<151; i++) {
        items.push({
          "JobID": "putItemsJobID" + i,
          "UserID": "putItemsUserID" + i,
          "name": "some job xyz",
          "other": "xyz abc"
        })
      }

      igthornDynamoDB.getItems(
        "Jobs_test_dev"
      )
      .then((data) => {
        assert.deepEqual(data, {
          Items: [],
          Count: 0,
          ScannedCount: 0
        })

        return igthornDynamoDB.putItems(
          "Jobs_test_dev",
          items
        );
      })
      .then((data) => {
        assert.deepEqual(data, [
          {
            "error": null,
            "data": {
              "UnprocessedItems": {}
            }
          },
          {
            "error": null,
            "data": {
              "UnprocessedItems": {}
            }
          }
        ]);

        return igthornDynamoDB.getItems(
          "Jobs_test_dev"
        );
      })
      .then((data) => {

        data.Items = _.sortBy(data.Items, 'JobID');

        assert.deepEqual(
          data,
          {
            Items: items,
            Count: 50,
            ScannedCount: 50
          }
        );

        done();
      })
      .catch((error) => {
        done(error);
      });
    });

  });

  describe("deleteItem", () => {

    it("should allow delete item (callback)", function (done) {

      this.timeout(0);

      igthornDynamoDB.putItems(
        "Jobs_test_dev",
        [
          {
            "UserID": "putItemsUserID101",
            "JobID": "putItemsJobID101"
          }
        ]
      )
      .then(() => {
        igthornDynamoDB.deleteItem(
          "Jobs_test_dev",
          {
            "UserID": "putItemsUserID101",
            "JobID": "putItemsJobID101"
          },
          {},
          (error, data) => {
            if(error) {
              throw error;
            }

            igthornDynamoDB.getItems(
              "Jobs_test_dev"
            )
            .then((data) => {
              assert.deepEqual(data, {
                Items: [],
                Count: 0,
                ScannedCount: 0
              });
              done();
            });
          }
        );
      });
    });

    it("should allow delete item (promise)", function (done) {

      this.timeout(0);

      igthornDynamoDB.putItems(
        "Jobs_test_dev",
        [
          {
            "UserID": "putItemsUserID101",
            "JobID": "putItemsJobID101"
          }
        ]
      )
      .then(() => {
        return igthornDynamoDB.deleteItem(
          "Jobs_test_dev",
          {
            "UserID": "putItemsUserID101",
            "JobID": "putItemsJobID101"
          }
        )
      })
      .then((data) => {
        return igthornDynamoDB.getItems(
          "Jobs_test_dev"
        );
      })
      .then((data) => {
        assert.deepEqual(data, {
          Items: [],
          Count: 0,
          ScannedCount: 0
        });
        done();
      })
      .catch((error) => {
        done(error);
      })
    });

  });

  describe("deleteItems", () => {

    it("should allow batch delete items (callback)", function (done) {

      this.timeout(0);

      igthornDynamoDB.connect(awsCredentails);

      const items = [];
      for(let i=1; i<51; i++) {
        items.push({
          "JobID": "putItemsJobID" + String("0" + i).slice(-2),
          "UserID": "putItemsUserID" + String("0" + i).slice(-2)
        })
      }

      igthornDynamoDB.putItems(
        "Jobs_test_dev",
        items
      )
      .then(() => {
        igthornDynamoDB.deleteItems(
          "Jobs_test_dev",
          items,
          {},
          (error, data) => {
            if(error) {
              throw error;
            }

            assert.deepEqual(data, [
              {
                "error": null,
                "data": {
                  "UnprocessedItems": {}
                }
              },
              {
                "error": null,
                "data": {
                  "UnprocessedItems": {}
                }
              }
            ]);

            igthornDynamoDB.getItems(
              "Jobs_test_dev"
            )
            .then((data) => {
              assert.deepEqual(data, {
                Items: [],
                Count: 0,
                ScannedCount: 0
              });
              done();
            });
          }
        )
      });
    });

    it("should allow batch delete items (promise)", function (done) {

      this.timeout(0);

      igthornDynamoDB.connect(awsCredentails);

      const items = [];
      for(let i=101; i<151; i++) {
        items.push({
          "JobID": "putItemsJobID" + i,
          "UserID": "putItemsUserID" + i
        })
      }

      igthornDynamoDB.putItems(
        "Jobs_test_dev",
        items
      )
      .then(() => {
        return igthornDynamoDB.deleteItems(
          "Jobs_test_dev",
          items
        );
      })
      .then((data) => {
        assert.deepEqual(data, [
          {
            "error": null,
            "data": {
              "UnprocessedItems": {}
            }
          },
          {
            "error": null,
            "data": {
              "UnprocessedItems": {}
            }
          }
        ]);
        return igthornDynamoDB.getItems(
        "Jobs_test_dev"
        )
      })
      .then((data) => {
        assert.deepEqual(data, {
          Items: [],
          Count: 0,
          ScannedCount: 0
        });
        done();
      })
      .catch((error) => {
        done(error);
      })
    });

  });

});
