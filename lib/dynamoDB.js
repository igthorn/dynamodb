
"use strict";

const AWS           = require("aws-sdk");
const marshalItem   = require("dynamodb-marshaler").marshalItem;
const unmarshalItem = require("dynamodb-marshaler").unmarshalItem;
const async         = require("async");

class IgthornDynamoDB
{
  constructor()
  {
    /**
     * Connections registry
     * @var object
     */
    this.connections = {};

    /**
     * Current connection's name in registry
     * @var AWS.DynamoDB|null
     */
    this.currectConnection = null;
  }

  connect(awsConfig, connectionName)
  {
    if(typeof awsConfig !== 'object') {
      throw new Error('Invalid AWS config provided. Object expected')
    }

    awsConfig.apiVersion = '2012-08-10';

    connectionName = connectionName ? connectionName : "main";

    if(this.connections[connectionName]) {
      return this.connections[connectionName];
    }

    const dynamodb = new AWS.DynamoDB(awsConfig);
    dynamodb.documentClient = new AWS.DynamoDB.DocumentClient(dynamodb);
    dynamodb.awsConfig = awsConfig;
    if(Object.keys(this.connections).length === 0) {
      this.currectConnection = dynamodb;
    }
    this.connections[connectionName] = dynamodb;
    return this;
  }

  use(connectionName)
  {
    if (typeof connectionName !== 'string') {
      throw new Error('Invalid connection name provided. Stirng expected');
    }

    if (!this.connections[connectionName]) {
      throw new Error('Unable to find connection using provided connection name');      
    }

    this.currectConnection = this.connections[connectionName];
    return this;
  }

  getConnection(connectionName)
  {
    if(!connectionName) {
      return this.currectConnection;
    }

    if (typeof connectionName !== 'string') {
      throw new Error('Invalid connection name provided. Stirng expected');
    }

    if (!this.connections[connectionName]) {
      throw new Error('Unable to find connection using provided connection name');      
    }

    return this.connections[connectionName];
  }

  describeTable(tableName, callback)
  {
    params = params ? params : {};

    if(typeof tableName !== 'string') {
      throw new Error('Invalid table name provided. String expected');
    }

    if(!this.currectConnection) {
      throw new Error('Unable to find current connection (DynamoDB service) - have you called connect() function?');
    }

    params.RequestItems = {
      tableName: tableName
    };

    callback = callback ? callback : function () {};

    return new Promise((resolve, reject) => {
      this.currectConnection.describeTable(params, (error, data) => {
        if(error) {
          reject(error);
          return callback(error);
        }

        resolve(data);
        return callback(null, data);
      });
    });
  }

  getItems(tableName, filters = {}, params, callback)
  {
    params = params ? params : {};

    if(typeof tableName !== 'string') {
      throw new Error('Invalid table name provided. String expected');
    }

    if(filters !== Object(filters)) {
      throw new Error('Invalid filters provided. Object expected');
    }

    if(params.TableName) {
      throw new Error(
        'Invalid getItems call. TableName is expected to be first argument, params argument is expected to don\'t contain "TableName" property'
      );
    }

    if(params.ScanFilter) {
      throw new Error(
        'Invalid getItems call. ScanFilter is expected to be second argument, params argument is expected to don\'t contain "ScanFilter" property'
      );
    }

    if(!this.currectConnection) {
      throw new Error('Unable to find current connection (DynamoDB service) - have you called connect() function?');
    }

    params.TableName  = tableName;
    params.ScanFilter = {};

    const comparisionOperator = [
      'EQ',
      'NE',
      'IN',
      'LE',
      'LT',
      'GE',
      'GT',
      'BETWEEN',
      'NOT_NULL',
      'NULL',
      'CONTAINS',
      'NOT_CONTAINS',
      'BEGINS_WITH'
    ];

    const keys = Object.keys(filters);
    for(let i=0; i<keys.length; i++) {
      const key   = keys[i];
      const condition = filters[key];

      if(condition !== Object(condition)) {
        throw new Error('Invalid condition provided for filter key: ' + key + '. Object expected');
      }

      const operator = Object.keys(condition).pop();

      if(comparisionOperator.indexOf(operator) === -1) {
        throw new Error('Invalid comparision operator provided for filter key: ' + key + '. Object expected');
      }

      params.ScanFilter[key] = {
        ComparisonOperator: operator,
        AttributeValueList: [
          marshalItem({value: condition[operator]}).value
        ]
      }
    }

    callback = callback ? callback : function () {};

    return new Promise((resolve, reject) => {
      this.currectConnection.scan(params, (error, data) => {
        if(error) {
          reject(error);
          return callback(error);
        }

        if(Array.isArray(data.Items)) {
          for(let i=0; i<data.Items.length; i++) {
            data.Items[i] = unmarshalItem(data.Items[i])
          }
        }

        resolve(data);
        return callback(null, data);
      });
    });
  }

  getItemByKeys(tableName, keys, params, callback)
  {
    params = params ? params : {};

    if(typeof tableName !== 'string') {
      throw new Error('Invalid table name provided. String expected');
    }

    if(keys !== Object(keys)) {
      throw new Error('Invalid keys provided. Object expected');
    }

    if(params.RequestItems) {
      throw new Error(
        'Invalid putItem call. RequestItems is expected to be first argument, params argument is expected to don\'t contain "RequestItems" property'
      );
    }

    if(!this.currectConnection) {
      throw new Error('Unable to find current connection (DynamoDB service) - have you called connect() function?');
    }

    params.RequestItems = {
      [tableName]: {
        Keys: [marshalItem(keys)]
      }
    };

    callback = callback ? callback : function () {};

    return new Promise((resolve, reject) => {
      this.currectConnection.batchGetItem(params, (error, data) => {
        if(error) {
          reject(error);
          return callback(error);
        }

        if(data && data.Responses && data.Responses[tableName] && Array.isArray(data.Responses[tableName])) {
          for(let i=0; i<data.Responses[tableName].length; i++) {
            data.Responses[tableName][i] = unmarshalItem(data.Responses[tableName][i]);
          }
        }

        resolve(data);
        return callback(null, data);
      });
    });
  }

  putItem(tableName, item, params, callback)
  {
    params = params ? params : {};

    if(typeof tableName !== 'string') {
      throw new Error('Invalid table name provided. String expected');
    }

    if(item !== Object(item)) {
      throw new Error('Invalid item provided. Object expected');
    }

    if(params.Item) {
      throw new Error(
        'Invalid putItem call. Item is expected to be first argument, params argument is expected to don\'t contain "Item" property'
      );
    }

    if(params.TableName) {
      throw new Error(
        'Invalid putItem call. TableName is expected to be second argument, params argument is expected to don\'t contain "TableName" property'
      );
    }

    if(!this.currectConnection) {
      throw new Error('Unable to find current connection (DynamoDB service) - have you called connect() function?');
    }

    params.TableName = tableName;
    params.Item = marshalItem(item);

    callback = callback ? callback : function () {};

    return new Promise((resolve, reject) => {
      this.currectConnection.putItem(params, (error, data) => {
        if(error) {
          reject(error);
          return callback(error);
        }

        resolve(data);
        return callback(null, data);
      });
    });
  }

  putItems(tableName, items, params, callback)
  {
    params = params ? params : {};

    if(typeof tableName !== 'string') {
      throw new Error('Invalid table name provided. String expected');
    }

    if(!Array.isArray(items)) {
      throw new Error('Invalid items list provided. Array of objects expected');
    }

    if(params.RequestItems) {
      throw new Error(
        'Invalid putItem call. Items is expected to be second argument, params argument is expected to don\'t contain "RequestItems" property'
      );
    }

    if(!this.currectConnection) {
      throw new Error('Unable to find current connection (DynamoDB service) - have you called connect() function?');
    }

    const requests = slice(items, this.currectConnection.awsConfig.maxBatchRequestSize || 25);

    for(let i=0; i<requests.length; i++) {
      const items = requests[i];

      for(let j=0; j<items.length; j++) {

        const item = items[j];

        if(item !== Object(item)) {
          throw new Error('Invalid item provided. Object expected');
        }

        requests[i][j] = {
          PutRequest: {
            Item: marshalItem(item)
          }
        };
      }
    }

    const results = [];

    callback = callback ? callback : function () {};

    return new Promise((resolve, reject) => {
      async.each(requests, (items, asyncCallback) => {

        params.RequestItems = {
          [tableName]: items
        };

        this.currectConnection.batchWriteItem(params, (error, data) => {
          results.push({
            error: error,
            data: data
          });
          if(error) {
            return asyncCallback(error);
          }
          asyncCallback();
        });

      }, (error) => {
        if(error) {
          reject(error);
          return callback(error);
        }

        resolve(results);
        callback(null, results);
      });
    });
  }

  deleteItem(tableName, keys, params, callback)
  {
    params = params ? params : {};

    if(typeof tableName !== 'string') {
      throw new Error('Invalid table name provided. String expected');
    }

    if(keys !== Object(keys)) {
      throw new Error('Invalid keys provided. Object expected');
    }

    if(params.TableName) {
      throw new Error(
        'Invalid putItem call. TableName is expected to be second argument, params argument is expected to don\'t contain "TableName" property'
      );
    }

    if(params.Key) {
      throw new Error(
        'Invalid putItem call. Items is expected to be second argument, params argument is expected to don\'t contain "Key" property'
      );
    }

    if(!this.currectConnection) {
      throw new Error('Unable to find current connection (DynamoDB service) - have you called connect() function?');
    }

    params.TableName = tableName;
    params.Key = {};
    const keysNames = Object.keys(keys);
    for(let i=0; i<keysNames.length; i++) {
      const keyName = keysNames[i];
      params.Key[keyName] = marshalItem({value: keys[keyName]}).value;
    }

    callback = callback ? callback : function () {};

    return new Promise((resolve, reject) => {
      this.currectConnection.deleteItem(params, (error, data) => {
        if(error) {
          reject(error);
          return callback(error);
        }

        resolve(data);
        return callback(null, data);
      });
    })
  }

  deleteItems(tableName, keys, params, callback)
  {
    params = params ? params : {};

    if(typeof tableName !== 'string') {
      throw new Error('Invalid table name provided. String expected');
    }

    if(!Array.isArray(keys)) {
      throw new Error('Invalid keys provided. Array of objects expected (each containing Key:value and possibly SecondaryKey:value)');
    }

    if(params.RequestItems) {
      throw new Error(
        'Invalid putItem call. Items is expected to be second argument, params argument is expected to don\'t contain "RequestItems" property'
      );
    }

    if(!this.currectConnection) {
      throw new Error('Unable to find current connection (DynamoDB service) - have you called connect() function?');
    }

    const requests = slice(keys, this.currectConnection.awsConfig.maxBatchRequestSize || 25);

    for(let i=0; i<requests.length; i++) {
      const listOfKeyValuePairs = requests[i];

      for(let j=0; j<listOfKeyValuePairs.length; j++) {

        const keyValuePairs = listOfKeyValuePairs[j];

        if(keyValuePairs !== Object(keyValuePairs)) {
          throw new Error('Invalid key pair provided. Object expected');
        }

        requests[i][j] = {
          DeleteRequest: {
            Key: marshalItem(keyValuePairs)
          }
        };
      }
    }

    const results = [];

    callback = callback ? callback : function () {};

    return new Promise((resolve, reject) => {
      async.each(requests, (items, asyncCallback) => {

        params.RequestItems = {
          [tableName]: items
        };

        this.currectConnection.batchWriteItem(params, (error, data) => {
          results.push({
            error: error,
            data: data
          });
          if(error) {
            return asyncCallback(error);
          }
          asyncCallback();
        });

      }, (error) => {
        if(error) {
          reject(error);
          return callback(error);
        }

        resolve(results);
        callback(null, results);
      });
    });
  }
}

const slice = (data, chunk) => {

  const result = [];
  for (let i=0, j=data.length; i<j; i+=chunk) {
      result.push(data.slice(i, i+chunk));
  }

  return result;
}

module.exports = new IgthornDynamoDB();
